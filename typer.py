import time
import curses
import sys
import getopt
import random
import curses.ascii

config = {
        'random_text': False,
        'n_words': 10,
        'dic_file': '',
        'backspace' : 'KEY_BACKSPACE',
        'menu_next' : '\t',
        'menu_prev' : 'KEY_BACKSPACE'
}

glob = {
        'SAVECONFIG' : False
}

def main(stdscr, argv):
    stdscr.clear()
    stdscr.refresh()
    curses.curs_set(0)

    read_config()
    display_logo(stdscr)
    set_opt(argv, stdscr)
    
    if glob['SAVECONFIG'] == True:
        export_config()
    
    #windows
    top = curses.newwin(1, curses.COLS, 0, 0)
    height = int((curses.LINES*2)/3)
    length = int((curses.COLS*2)/3)
    height_pad = int((curses.LINES - height)/2)
    length_pad = int((curses.COLS - length)/2)
    text_area = curses.newwin(height, length, height_pad, length_pad)
    menu_heigt = 5
    menu_length = 30
    menu_heigt_pad = int((curses.LINES - menu_heigt)/2)
    menu_length_pad = int((curses.COLS - menu_length)/2)
    wpm_pad = 4

    #text setup
    if config['dic_file'] == '':
        stdscr.addstr("typer.py -f <file>")
        stdscr.getkey()
        sys.exit(2)

    if config['random_text']:
        text = random_text_setup(config['dic_file'], config['n_words'])
    else:
        text = text_setup(config['dic_file'],config['n_words'])

    #first print
    #TODO: evitare che le parole vengano interrotte a meta
    display_text(text_area, text, length-20, 0, '') 
    top.addstr(0,0,'wpm: ', curses.A_REVERSE)
    top.refresh()
    
    #color pairs
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    
    #variables
    acc = stdscr.getkey()
    stdscr.nodelay(True)
    start_time = int(time.time())
    word = 0
    wpm = 0
    seconds = 0

    while True:
        seconds = round(time.time() - start_time)
        if seconds != 0:
            wpm = round(60*len([x for x in text if x['color'] == curses.color_pair(2)])/seconds)
        try:
            key = stdscr.getkey()
        except:
            display_top(top, wpm, wpm_pad, seconds)
            continue

        if key == ' ': 
            if text[word]['txt'] == acc:
                text[word]['color'] = curses.color_pair(2)
            else:
                text[word]['color'] = curses.color_pair(1)
            
            acc = ''
            word = word + 1
            if word >= len(text):
                break
        elif key == '\t':
            start_time = pause(menu_heigt, menu_length, menu_heigt_pad, menu_length_pad, start_time, seconds)
        elif key == config['backspace']:
            acc = acc[0:-1]
        elif len(key) == 1 and curses.ascii.isgraph(ord(key)):
            acc = acc+key
        display_text(text_area, text, length-20, word, acc)
        display_top(top, wpm, wpm_pad, seconds)
    stdscr.nodelay(False)
    show_result(menu_heigt,menu_length,menu_heigt_pad, menu_length_pad, wpm, seconds)

def display_logo(stdscr):
    logo = [
    '████████╗██╗░░░██╗██████╗░███████╗██████╗░',
    '╚══██╔══╝╚██╗░██╔╝██╔══██╗██╔════╝██╔══██╗',
    '░░░██║░░░░╚████╔╝░██████╔╝█████╗░░██████╔╝',
    '░░░██║░░░░░╚██╔╝░░██╔═══╝░██╔══╝░░██╔══██╗',
    '░░░██║░░░░░░██║░░░██║░░░░░███████╗██║░░██║',
    '░░░╚═╝░░░░░░╚═╝░░░╚═╝░░░░░╚══════╝╚═╝░░╚═╝']

    length_pad = int((curses.COLS - 42)/2)
    height_pad = int((curses.LINES - 6)/2)
    for i in logo:
        stdscr.addstr(height_pad, length_pad,i)
        height_pad = height_pad + 1
    stdscr.getkey()
    stdscr.clear()
    stdscr.refresh()
def set_opt(argv, stdscr):
    opts, args = getopt.getopt(argv, "hrbef:w:", ["file=", "words="])
    for opt, arg in opts:
        if opt == "-h":
            stdscr.addstr("typer.py -f <file> [OPTIONS]\n")
            stdscr.addstr("-w N\tprint N words from the file\tdefault: 10\n")
            stdscr.addstr("-r\twords are selected randomly\tdefault: False\n")
            stdscr.addstr("-b\tset a different backspace char\n")
            stdscr.addstr("-e\texport current settings to config file\n")
            stdscr.addstr("-h\tshow this page and exit\n")
            stdscr.getkey()
            sys.exit()
        elif opt == "-b":
            config['backspace'] = ask_backspace(stdscr)
        elif opt in ("-f", "--file"):
            config['dic_file'] = arg
        elif opt == "-r":
            config['random_text'] = True;
        elif opt in ("-w", "--words"):
            try:
                config['n_words'] = int(arg) 
            except ValueError:
                stdscr.addstr("argument -w should be an integer")
                stdscr.getkey()
                sys.exit(2)
        elif opt == "-e":
            glob['SAVECONFIG'] = True

def export_config():
    try:
        with open('.typerrc', 'w') as f:
            for i in config:
                if type(config[i]) == type(''):
                    f.write('str '+i+' '+config[i]+'\n')
                elif type(config[i]) == type(1):
                    f.write('int '+i+' '+str(config[i])+'\n')
                elif type(config[i]) == type('True'):
                    f.write('bool '+i+str(config[i])+'\n')
    except:
        exit(2)
                
def ask_backspace(stdscr):
    stdscr.addstr("type a backspace")
    stdscr.refresh()
    return stdscr.getkey()

def random_text_setup(dictionary, num):
    with open(dictionary) as dic:
        r = ""
        for w in dic.readlines():
            r = r + w
        words = r.split()
        text = []
        for n in range(num):
            text.append({'txt': words[random.randrange(len(words))], 'color': 0})
        return text

def text_setup(dictionary, num):
    with open(dictionary) as dic:
        r = ""
        for w in dic.readlines():
            r = r + w
        words = r.split()
        text = []
        start = random.randrange(len(words) - num)
        for n in range(start, start+num):
            text.append({'txt':words[n], 'color':0})
    return text

def pause(menu_heigt, menu_length, menu_heigt_pad, menu_length_pad, start_time, seconds):
    menu = curses.newwin(menu_heigt, menu_length, menu_heigt_pad, menu_length_pad)
    menu.border()
    menu.move(1,1)
    menu.addstr('PAUSE')
    menu.nodelay(False)
    selected = 0
    n_items = 2
    items = ['resume', 'exit']
    while True:
        menu.refresh()
        items = ['resume', 'exit']
        menu.move(2,1)
        for i in range(0,n_items):
            if(i == selected):
                menu.addstr(items[i], curses.A_REVERSE)
                menu.addstr('\t')
            else:
                menu.addstr(items[i]+'\t')
        tmp = menu.getkey()
        if tmp == config['menu_next']:
            selected = (selected + 1)%n_items
        elif tmp == config['menu_prev']:
            selected = (selected - 1) % n_items
        elif tmp == '\n':
            if selected == 0:
                break
            if selected == 1:
                exit()
    menu.clear()
    menu.refresh()
    return time.time() - seconds

def show_result(menu_heigt,menu_length,menu_heigt_pad, menu_length_pad, wpm, seconds):
    res = curses.newwin(menu_heigt, menu_length, menu_heigt_pad, menu_length_pad)
    res.border()
    res.move(1,1)
    res.addstr("wpm: " + str(wpm))
    res.move(2,1)
    res.addstr("seconds: " + str(seconds))
    res.refresh()
    res.nodelay(False)
    res.getkey()
    exit()

def display_top(top, wpm, wpm_pad, seconds):
    top.addstr(0, wpm_pad, str(wpm)+'     sec: '+str(seconds)+'    ', curses.A_REVERSE)
    top.refresh()

def display_text(text_area, text, length, word, acc):
    text_area.move(0,0)
    count = 0
    x = 0
    y = 0
    for n in range(0, len(text)):
        t = text[n]
        count += len(t['txt'])
        if count > length:
            text_area.addstr('\n')
            count = 0
        if n == word:
            y,x = text_area.getyx()
        text_area.addstr(t['txt']+' ', t['color'])
    text_area.move(y, x)
        
    if text[word]['txt'][0:len(acc)] == acc:
        text_area.addstr(acc, curses.color_pair(3))
    else:
        text_area.addstr(acc, curses.color_pair(1))
    if len(acc) >= len(text[word]['txt']):
        ch = ' '
    else: 
        ch = text[word]['txt'][len(acc)]
    text_area.addstr(ch, curses.A_REVERSE)
    text_area.refresh()

def read_config():
    try:
        with open('.typerrc') as f:
            for c in f.readlines():
                exec_command(c)
    except:
        return

def exec_command(command):
    array = command.split();
    if array[0] == 'int':
        try:
            config[array[1]] = int(array[2])
        except ValueError:
            print("error in config file: "+ command)
    elif array[0] == 'str':
        config[array[1]] = array[2]
    elif array[0] == 'rgb':
        try:
            confid[array[1]] = [int(array[1]), int(array[2]), int(array[3])]
        except ValueError:
            print("error in config file: "+ command)

if __name__ == "__main__":
    curses.wrapper(main, sys.argv[1:])
